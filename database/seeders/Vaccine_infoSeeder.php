<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Str;

class Vaccine_infoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'2',
            'province'=>'preah vihear',
            'card_type'=> 'MOD',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'5',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'4',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'3',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'Kandal',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'Kandal',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'Kandal',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'1',
            'province'=>'preah vihear',
            'card_type'=> 'MOD',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'5',
            'province'=>'phnom penh',
            'card_type'=> 'MOH',
        ]);
        DB::table('vaccine_info')->insert([
            'user_name' => Str::random(10),
            'doses'=>'5',
            'province'=>'phnom penh',
            'card_type'=> 'MOD',
        ]);
    }
}
