<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VaccineInfo;
use DB;

class VaccineInfoController extends Controller
{
    public function index(){
        $report = VaccineInfo::all();
        $provinces = VaccineInfo::select('province')->groupBy('province')->get();
        $doses = VaccineInfo::select('doses')->groupBy('doses')->get();
        return view('vaccine_info.index',compact('report','provinces','doses'));
    }
    public function filter(Request $request){
        $province = $request->province;
        $doses = $request->doses;
        $search = $request->search;

        $covaccines = VaccineInfo::query();
        
        if(!empty($province)){
            $covaccines->where('province','=',$province);

        }
        if(!empty($doses)){
            $covaccines->where('doses','=',$doses);
        }
        if(!empty($search)){
            $covaccines->where('province','like', '%' . $search . '%')->orWhere('doses','like', '%' . $search . '%');
        }
        $vaccines = $covaccines->select('province','doses','card_type',DB::raw('count(*) as total'))->groupBy('card_type','province','doses')->get();
        
        return response()->json($vaccines);
    }
}
