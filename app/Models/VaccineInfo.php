<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VaccineInfo extends Model
{
    use HasFactory;
    protected $table = 'vaccine_info';

    protected $fillable = [
        'user_name',
        'doses',
        'province',
        'card_type',
    ];
    public function scopeFilter($vaccine_info,$search)
    {
        return $vaccine_info->where('province', 'like', '%' . $search . '%')->orWhere('doses', 'like', '%' . $search . '%');
    }
}
